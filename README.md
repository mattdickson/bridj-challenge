# bridj-challenge

> Bridj Coding Challenge

If NPM is installed the simplest way to run is to use `npm run dev` in the project folder and the open localhost:8080

If there is a standard Http Server you can run the built app by moving the files from `dist` to the server which will allow it to run

To build use `npm run build` in the project folder which will output to the `dist` folder

To run a simple unit test with NPM use 'npm run test` in the project folder