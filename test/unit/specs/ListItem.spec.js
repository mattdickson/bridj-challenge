import Vue from 'vue'
import ListItem from '@/components/ListItem'

var testInfo = {
  'name': 'Othello',
  'date': '2017-11-07T20:00:00Z',
  'available_seats': 123,
  'price': 75.0,
  'venue': 'Awesome Theatre',
  'labels': ['play', 'shakespeare']
}

describe('ListItem.vue', () => {
  it('Render Name Correctly', () => {
    const Constructor = Vue.extend(ListItem)

    const vm = new Constructor({ propsData: {listInfo: testInfo} }).$mount()
    vm._watcher.run()
    expect(vm.$el.querySelector('#eventName').textContent)
      .to.equal('Othello')
  })

  it('Render Available Seats Correctly', () => {
    const Constructor = Vue.extend(ListItem)

    const vm = new Constructor({ propsData: {listInfo: testInfo} }).$mount()
    vm._watcher.run()
    expect(vm.$el.querySelector('#eventSeats').textContent)
      .to.equal('123')
  })

  it('Render Price Correctly', () => {
    const Constructor = Vue.extend(ListItem)

    const vm = new Constructor({ propsData: {listInfo: testInfo} }).$mount()
    vm._watcher.run()
    expect(vm.$el.querySelector('#eventPrice').textContent)
      .to.equal('$75')
  })
})
